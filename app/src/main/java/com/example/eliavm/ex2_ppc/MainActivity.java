package com.example.eliavm.ex2_ppc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private static final String MESSAGES = "messages";
    ImageButton sendButton;
    RecyclerView mRec;
    MessageAdapter messageAdapter;
    EditText userMessage;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        ArrayList<String> messages = new ArrayList<>(messageAdapter.messages.size());
        for (Message m : messageAdapter.messages)
        {
            messages.add(m.getUser());
            messages.add(m.getMessage());
            messages.add(m.getTimestamp().toString());
        }
        outState.putStringArrayList(MESSAGES, messages);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRec = findViewById(R.id.messageRecycler);
        ArrayList<Message> input =  getInput(savedInstanceState);

        messageAdapter = new MessageAdapter(input);
        mRec.setAdapter(messageAdapter);
        mRec.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        userMessage = findViewById(R.id.inputRaw);

        userMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                sendButton.setEnabled(!TextUtils.isEmpty(s));
            }
        });

        sendButton = findViewById(R.id.sendButton);
        // on click, create a message object and add it to messages.
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(userMessage.getText());
                Message m = new Message(userMessage.getText().toString(), ": Eliav", System.currentTimeMillis());
                messageAdapter.addMsg(m);
                userMessage.setText("");
            }
        });
    }

    private ArrayList<Message> getInput(Bundle savedInstanceState) {
        if (savedInstanceState == null || savedInstanceState.get(MESSAGES) == null)
        {
            return new ArrayList<>();
        }
        ArrayList<Message> newMessages = new ArrayList<>();
        ArrayList<String> messages = savedInstanceState.getStringArrayList(MESSAGES);
        int counter = 0;
        String user = "";
        String msg = "";
        Long timestamp = 1L;
        for (String info : messages)
        {
            if (counter == 0)
            {
                user = info;
                counter += 1;
            }
            else if (counter == 1)
            {
                msg = info;
                counter += 1;
            }
            else if (counter == 2)
            {
                timestamp = Long.parseLong(info);
                counter = 0;
                newMessages.add(new Message(msg, user, timestamp));
            }
        }
        return newMessages;
    }
}
