package com.example.eliavm.ex2_ppc;

/**
 * The class represents a message in a chat.
 */

public class Message {

    private String message;
    private String user;
    private Long timestamp;

    public Message(String message, String user, Long timestamp)
    {
        this.message = message;
        this.user = user;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getUser() {
        return user;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}
