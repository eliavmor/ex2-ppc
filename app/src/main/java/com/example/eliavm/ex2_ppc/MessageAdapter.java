package com.example.eliavm.ex2_ppc;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * The class represents message adapter in chat application
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    final List<Message> messages;

    public MessageAdapter(List<Message> data)
    {
        this.messages = data;
    }

    public void addMsg(Message m)
    {
        messages.add(m);
        notifyItemInserted(messages.size() - 1);
    }

    @Override
    public MessageAdapter.MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.message_layout, parent, false);
        MessageViewHolder viewHolder = new MessageViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MessageAdapter.MessageViewHolder holder, int position)
    {
        Message message = messages.get(position);
        holder.timestamp.setText(DateUtils.getRelativeTimeSpanString(holder.itemView.getContext(), message.getTimestamp()));
        holder.message.setText(message.getMessage());
        holder.user.setText(message.getUser());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    static class MessageViewHolder extends RecyclerView.ViewHolder
    {
        TextView user;
        TextView message;
        TextView timestamp;

        public MessageViewHolder(View itemView) {
            super(itemView);
            user = itemView.findViewById(R.id.userName);
            message = itemView.findViewById(R.id.user_message);
            timestamp = itemView.findViewById(R.id.timestamp);
        }
    }
}
